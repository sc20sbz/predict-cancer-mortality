# Predict Cancer Mortality Rates in US Counties


## Description

The provided dataset comprises data collected from multiple counties in the US. The regression task for this assessment is to predict cancer mortality rates in "unseen" US counties, given some training data. The training data ('Training_data.csv') comprises various features/predictors related to socio-economic characteristics, amongst other types of information for specific counties in the country. The corresponding target variables for the training set are provided in a separate CSV file ('Training_data_targets.csv'). 

Tasks: 
1) Plot histograms of all features to visualise their distributions and identify outliers. Compute correlations of all features with the target variable (across the data set) and sort them according the strength of correlations.
2) Create an ML pipeline using scikit-learn to pre-process the training data.
3) Fit linear regression models to the pre-processed data using: Ordinary least squares (OLS), Lasso and Ridge models. Choose suitable regularisation weights for Lasso and Ridge regression.
4) Use Lasso regression and the best regularisation weight identified from Exercise 3 to identify the five most important/relevant features for the provided data set and regression task. 
5) Fit a Random Forest regression model to the training data and quantitatively evaluate and compare the Random Forest regression model with the best linear regression model identified from Exercise 3. 
6) Use the provided test example data ('Test_data_example.csv' and 'Test_data_example_targets.csv') to write an inference script to evaluate the best regression model identified from preceding exercises.

## Authors and acknowledgment
Suriana
University of Leeds


## Project status
Project has successfully been completed